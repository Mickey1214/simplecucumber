const {Given, Then, When} = require('@cucumber/cucumber'); 
const assert = require('assert');

let a, b, t;

Given('numbers {int} and {int}', function(iI1, iI2){
    a = iI1;
    b = iI2;
});

When('they are added together', function(){
    t = a + b;
});

Then('the total should be {int}', function(iResult){
    assert.strictEqual(t, iResult);
});